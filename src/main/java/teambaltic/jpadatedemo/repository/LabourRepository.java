package teambaltic.jpadatedemo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import teambaltic.jpadatedemo.entity.Labour;

@Repository
public interface LabourRepository  extends JpaRepository<Labour, Integer> {

	@Override
    List<Labour> findAll();
	@Override
    Optional<Labour> findById      ( Integer id );
	Optional<Labour> findByWorker  ( String  worker );
	List<Labour>     findBySqlDate ( java.sql.Date  sqlDate);
    List<Labour>     findByUtilDate( java.util.Date utilDate);

}
