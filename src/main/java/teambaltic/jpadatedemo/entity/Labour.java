package teambaltic.jpadatedemo.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity( name = "labour" )
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Labour {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column( name = "worker")
	private String worker;

	@Column
	private Float hours;

	@Column
	@Basic
	private java.sql.Date sqlDate;

    @Column
    @Basic
    @Temporal(TemporalType.DATE)
    private java.util.Date utilDate;
}
