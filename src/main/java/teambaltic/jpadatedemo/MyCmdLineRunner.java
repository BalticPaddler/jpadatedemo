package teambaltic.jpadatedemo;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import teambaltic.jpadatedemo.entity.Labour;
import teambaltic.jpadatedemo.repository.LabourRepository;

@Component
public class MyCmdLineRunner implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(MyCmdLineRunner.class);

	@Autowired
	private LabourRepository repository;

	@Override
	public void run(final String... args) throws Exception {

		repository.findAll().forEach((labour) -> {
			logger.info("{}", labour);
		});

		final Optional<Labour> foundById = repository.findById(3);
		logger.info("By id (3): {}", foundById.get() );
		final Optional<Labour> foundByWorker = repository.findByWorker( "Sarah Wagenknecht" );
		logger.info("By worker (Sarah Wagenknecht): {}", foundByWorker.get() );
		final java.sql.Date sqlDate = foundById.get().getSqlDate();
		logger.info("sqlDate =  {}", sqlDate);
		final List<Labour> foundBySqlDate = repository.findBySqlDate( sqlDate );
		if( foundBySqlDate.isEmpty() ) {
			logger.warn("HOLY SQL SHIT!!");
		} else {
			logger.info("By sqlDate {}: {}", sqlDate, foundBySqlDate.get(0) );
		}

		final java.util.Date utilDate = foundById.get().getUtilDate();
        logger.info("utilDate =  {}", utilDate);
        final List<Labour> foundByUtilDate = repository.findByUtilDate( utilDate );
        if( foundByUtilDate.isEmpty() ) {
            logger.warn("HOLY UTIL SHIT!!");
        } else {
            logger.info("By utilDate {}: {}", utilDate, foundByUtilDate.get(0) );
        }
	}
}
