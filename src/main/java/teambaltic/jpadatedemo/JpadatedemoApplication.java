package teambaltic.jpadatedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpadatedemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpadatedemoApplication.class, args);
	}

}
