/**
 * SQL.java
 *
 * Created on 12.01.2021
 * by <a href="mailto:mhw@teambaltic.de">Mathias-H.&nbsp;Weber&nbsp;(MW)</a>
 *
 * Coole Software - Mein Beitrag im Kampf gegen die Klimaerwärmung!
 *
 * Copyright (C) 2021 Team Baltic. All rights reserved
 */
// ############################################################################
package teambaltic.jpadatedemo.utils.sql;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.identity.IdentityColumnSupportImpl;

// ############################################################################
public class SQLiteDialectIdentityColumnSupport extends IdentityColumnSupportImpl {

    public SQLiteDialectIdentityColumnSupport(final Dialect dialect){
//            super(dialect);
            super();
        }

    @Override
    public boolean supportsIdentityColumns() {
        return true;
    }

  /*
    public boolean supportsInsertSelectIdentity() {
    return true; // As specified in NHibernate dialect
  }
  */

    @Override
    public boolean hasDataTypeInIdentityColumn() {
        // As specified in NHibernate dialect
        // FIXME true
        return false;
    }

  /*
    public String appendIdentitySelectToInsert(String insertString) {
    return new StringBuffer(insertString.length()+30). // As specified in NHibernate dialect
      append(insertString).
      append("; ").append(getIdentitySelectString()).
      toString();
  }
  */

    @Override
    public String getIdentitySelectString(final String table, final String column, final int type) {
        return "select last_insert_rowid()";
    }

    @Override
    public String getIdentityColumnString(final int type) {
        // return "integer primary key autoincrement";
        // FIXME "autoincrement"
        return "integer";
    }
}// ############################################################################
